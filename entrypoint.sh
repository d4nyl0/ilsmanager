#!/bin/sh

# Be sure to be in the right place.
if ! [ -f ".env" ]; then
	echo "ERROR: Missing .env file. That file MUST be available before starting the containers."
	exit 1
fi

# Hardening.
chmod o= .env

IS_FIRST_TIME=
RUN_COMPOSER_INSTALL=
if ! [ -f "./vendor" ]; then
	IS_FIRST_TIME=1
	RUN_COMPOSER_INSTALL=1
fi

# Run Composer if never executed.
if [ "$RUN_COMPOSER_INSTALL" = 1 ]; then
	composer install
fi

# Run Artisan stuff if never done before.
if [ "$IS_FIRST_TIME" = 1 ]; then
	php artisan migrate
	php artisan key:generate
	php artisan db:seed
fi

# Import environment variables to show something nice to the user.
if [ -f ./.env ]; then
	. ./.env
fi

echo " __________"
echo "< Running! >"
echo " ----------"
echo "        \   ^__^"
echo "         \  (oo)\_______"
echo "            (__)\       )\/\ "
echo "                ||----w |"
echo "                ||     ||"

if [ -n "$ILSMANAGER_EXTERNAL_PORT" ]; then

	# Generate a nice "Visit this address <link>" whatever the Docker port.
	# For interesting reasons the "port" in Docker may contain an hostname or not.
	# Is the string needing a dummy hostname?
	ILSMANAGER_EXTERNAL_HOST=
	if [ "${ILSMANAGER_EXTERNAL_PORT#*":"}" = "$ILSMANAGER_EXTERNAL_PORT" ]; then
		ILSMANAGER_EXTERNAL_HOST="localhost:"
	fi

	echo "Visit this address:"
	echo "  http://$ILSMANAGER_EXTERNAL_HOST""$ILSMANAGER_EXTERNAL_PORT""/"
fi

echo
echo "  Edit the file .env to change the external port (ILSMANAGER_EXTERNAL_PORT)."
echo
echo "(Press CTRL+C to quit)"
echo
echo "IMPORTANT: Ignore the port mentioned under this message :)"

echo "(Press CTRL+C to quit)"

# From inside the Container, listen on all interfaces.
# Then the user can change the external port form the compose.yaml.
# Output ignored to avoid confusion. Since "8000" is just the internal port.
# and the container is not aware of the external port.
php artisan serve --host=0.0.0.0 --port=8000 > /dev/null
