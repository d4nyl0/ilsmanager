FROM debian:bookworm

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get install --yes \
      composer \
      apache2 \
      libapache2-mod-php \
      php-cli \
      php-mysql \
      php-pdo \
      php-dom \
      php-curl \
 && apt-get clean

RUN groupadd --gid 1000                          user
RUN useradd  --uid 1000 --create-home --gid 1000 user

COPY ./entrypoint.sh /entrypoint.sh

USER user:user

RUN mkdir --parents /home/user/.composer

WORKDIR /var/www/html

EXPOSE 8000/tcp

ENTRYPOINT [ "/entrypoint.sh" ]
