<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        'App\User' => 'App\Policies\UserPolicy',
        'App\Section' => 'App\Policies\SectionPolicy',
        'App\Bank' => 'App\Policies\BankPolicy',
        'App\Movement' => 'App\Policies\MovementPolicy',
        'App\Receipt' => 'App\Policies\ReceiptPolicy',
        'App\Sponsor' => 'App\Policies\SponsorPolicy',
    ];

    public function boot()
    {
        $this->registerPolicies();
    }
}
