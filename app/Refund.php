<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

class Refund extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function movement()
    {
        return $this->belongsTo('App\Movement');
    }

    public function getPrintableDescriptionAttribute()
    {
        return sprintf('%s € - %s - %s', $this->amount, $this->user ? $this->user->printable_name : '?', $this->section ? 'Sezione Locale di ' . $this->section->city : 'Nessuna Sezione Locale');
    }

    public function getAttachmentsPathAttribute()
    {
        return storage_path('refunds/' . $this->id);
    }

    public function getAttachmentsAttribute()
    {
        $ret = new Collection();

        $path = $this->attachments_path;
        if (file_exists($path)) {
            $files = scandir($path);

            foreach($files as $f) {
                if ($f[0] == '.')
                    continue;

                $ret->push($f);
            }
        }

        return $ret;
    }

    public function attachFile($file)
    {
        $file->move($this->attachments_path, $file->getClientOriginalName());
    }
}
