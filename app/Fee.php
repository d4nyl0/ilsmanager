<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    public function account_row()
    {
        return $this->belongsTo('App\AccountRow');
    }

    public function receipt()
    {
        $row = $this->account_row;
        if ($row)
            return $row->movement->receipt;
        else
            return null;
    }
}
