<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EconomicLog extends Model
{
    public function account_row()
    {
        return $this->belongsTo('App\AccountRow');
    }
}
