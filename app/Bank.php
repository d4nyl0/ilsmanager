<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    public function movements()
    {
        return $this->hasMany('App\Movement');
    }

    public function rules()
    {
        return $this->hasMany('App\AccountRule');
    }

    public static function types()
    {
        return [
            (object) [
                'identifier' => 'paypal',
                'label' => 'PayPal'
            ],
            (object) [
                'identifier' => 'unicredit',
                'label' => 'Unicredit Banca'
            ],
        ];
    }

    public function getLastUpdateAttribute()
    {
        /*
            In fase di importazione degli estratti conto PayPal creo un
            movimento per tenere traccia delle commissioni, che però non deve
            essere considerato come ultimo movimento
        */
        if ($this->type == 'paypal') {
            return $this->movements()->where('notes', '!=', 'Commissioni PayPal')->max('date');
        }
        else {
            return $this->movements()->max('date');
        }
    }
}
