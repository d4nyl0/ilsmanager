<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\User;

abstract class EditController extends Controller
{
    protected $classname;
    protected $view_folder;

    protected function init($options)
    {
        $this->classname = $options['classname'];
        $this->view_folder = $options['view_folder'];
    }

    public function index()
    {
        $query = ($this->classname)::orderBy($this->defaultSortingColumn(), 'asc');
        $query = $this->query($query);
        $objects = $query->get();
        return view($this->view_folder . '.index', compact('objects'));
    }

    public function store(Request $request)
    {
        $this->authorize('create', $this->classname);
        $validations = $this->defaultValidations(null);
        $request->validate($validations);

        $object = new $this->classname;
        $object = $this->requestToObject($request, $object);
        $object->save();
        $this->afterSaving($request, $object);

        return redirect()->route($this->view_folder . '.index');
    }

    public function show($id)
    {
        $object = ($this->classname)::find($id);
        $this->authorize('view', $object);
        return view($this->view_folder . '.edit', compact('object'));
    }

    public function edit($id)
    {
        $object = ($this->classname)::find($id);
        $this->authorize('update', $object);
        return view($this->view_folder . '.edit', compact('object'));
    }

    public function update(Request $request, $id)
    {
        $object = ($this->classname)::find($id);
        $this->authorize('update', $object);

        $validations = $this->defaultValidations($object);
        $request->validate($validations);

        $object = $this->requestToObject($request, $object);
        $object->save();
        $this->afterSaving($request, $object);

        return redirect()->route($this->view_folder . '.edit', $id);
    }

    public function destroy($id)
    {
        $object = ($this->classname)::find($id);
        $this->authorize('delete', $object);
        $object->delete();
        return redirect()->route($this->view_folder . '.index');
    }

    protected function fitObject($object, $fields, $request)
    {
        foreach($fields as $f) {
            if ($request->has($f)) {
                $object->$f = $request->input($f);
            }
        }

        return $object;
    }

    protected function defaultSortingColumn()
    {
        return 'name';
    }

    protected function query($query, $id = null)
    {
        return $query;
    }

    protected function afterSaving($request, $object)
    {
        return;
    }

    protected abstract function defaultValidations($object);
    protected abstract function requestToObject($request, $object);
}
