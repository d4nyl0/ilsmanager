<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Votation;
use App\Vote;

class VoteController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Vote',
            'view_folder' => 'vote'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'votation_id' => 'required',
        ];
    }

    protected function requestToObject($request, $object)
    {
        $object = null;

        $user = $request->user();
        $votation_id = $request->input('votation_id');

        $votation = Votation::find($votation_id);

        foreach($votation->assembly->delegations($user) as $voter) {
            $vote = Vote::where('user_id', $voter->id)->where('votation_id', $votation_id)->first();
            if (is_null($vote)) {
                $vote = new Vote();
                $vote->user_id = $voter->id;
                $vote->votation_id = $votation->id;
            }

            $vote->answer = array_filter($request->input('vote_' . $voter->id, []));

            if (count($vote->answer) > $votation->max_options) {
                Log::error('Non salvo voto: numero di scelte superiore a quello concesso');
            }
            else {
                $vote->save();
            }

            $object = is_null($object) ? $vote : $object;
        }

        return $object;
    }

    public function index()
    {
        return redirect()->route('assembly.index');
    }

    public function edit($id)
    {
        return redirect()->route('assembly.index');
    }
}
