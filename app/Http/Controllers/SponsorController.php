<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Auth;
use Hash;
use Log;

use App\Section;
use App\User;
use App\Config;

class SponsorController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Sponsor',
            'view_folder' => 'sponsor'
        ]);
    }

    protected function requestToObject($request, $object)
    {
        $fields = ['name', 'website', 'email', 'expiration', 'notes'];
        $object = $this->fitObject($object, $fields, $request);
        return $object;
    }

    protected function defaultValidations($object)
    {
        $ret = [
            'name' => 'required|max:255',
            'website' => 'required|max:255',
            'email' => 'required|email|max:255',
            'expiration' => 'required|max:255',
        ];

        return $ret;
    }

    protected function afterSaving($request, $object)
    {
        if ($request->hasFile('logo')) {
            $logo = $request->file('logo');
            $object->attachFile($logo);
        }
    }

    public function logo($id)
    {
        $path = $path = storage_path('sponsors/' . $id);
        if (file_exists($path)) {
            return response()->download($path);
        }
        else {
            return '';
        }
    }
}
