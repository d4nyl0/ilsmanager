<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Michelf\Markdown;

class DocumentationController extends Controller
{
    public function index(Request $request)
    {
        $folder = $request->input('folder', '');
        if ($folder == '/')
            $folder = '';

        $path = storage_path('documentation' . $folder);
        $dir = scandir($path);
        $files = [];

        foreach($dir as $f) {
            if ($f[0] == '.')
                continue;

            $files[] = (object) [
                'name' => $f,
                'directory' => is_dir($path . '/' . $f),
                'ext' => pathinfo($path . '/' . $f, PATHINFO_EXTENSION)
            ];
        }

        return view('documentation.index', compact('folder', 'files'));
    }

    public function store(Request $request)
    {
        $this->checkAuth();

        $folder = $request->input('folder', '');
        if ($folder == '/')
            $folder = '';

        $path = storage_path('documentation' . $folder);

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $file->move($path, $file->getClientOriginalName());
        }
        else {
            $name = $request->input('name');
            mkdir($path . '/' . $name, 0770);
        }

        return redirect()->route('doc.index', ['folder' => $folder]);
    }

    public function download(Request $request)
    {
        $file = $request->input('file');
        $file = str_replace('..', '', $file);
        $path = storage_path('documentation' . $file);
        return response()->download($path);
    }

    public function link(Request $request)
    {
        $file = $request->input('file');
        $file = str_replace('..', '', $file);
        $path = storage_path('documentation' . $file);
        return response()->file($path);
    }

    public function preview(Request $request)
    {
        $folder = $request->input('folder', '');
        if ($folder == '/')
            $folder = '';
        
        $file = substr($folder, 1);
        $path = storage_path('documentation' . $folder);
        if (file_exists($path) ) {
            if ( mime_content_type($path) === 'text/plain') {
                $text = file_get_contents($path);
                $content = Markdown::defaultTransform($text);
            } elseif ( mime_content_type($path) === 'text/html') {
                $text = file_get_contents($path);
                $content = str_replace( "\n", "<br>", strip_tags($text) );
            } elseif ( mime_content_type($path) === 'application/pdf') {
                $content = "<iframe src='" . e( route('doc.link', ['file' => '/' . $file]) ) . "' width='100%' height='500px'></iframe>";
            } else {
                $content = 'Questo non è un file visualizzabile valido.';
                $file = '404';
            }

            return view('documentation.preview', compact('content', 'file', 'folder'));
        }
        
        $content = 'Questo non è un file visualizzabile valido.';
        $file = '404';
        return view('documentation.preview', compact('content', 'file', 'folder'));
    }
}
