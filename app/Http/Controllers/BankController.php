<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BankController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Bank',
            'view_folder' => 'bank'
        ]);
    }

    protected function defaultValidations($object)
    {
        return [
            'name' => 'required|max:255',
            'identifier' => 'required|max:255',
        ];
    }

    protected function requestToObject($request, $object)
    {
        $object->name = $request->input('name');
        $object->type = $request->input('type');
        $object->identifier = $request->input('identifier');
        return $object;
    }
}
