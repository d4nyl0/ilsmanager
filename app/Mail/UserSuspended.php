<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserSuspended extends Mailable
{
    use Queueable, SerializesModels;

    private $user = null;

    public function __construct($user)
    {
        $this->user = $user;
    }

    public function build()
    {
        return $this->subject('Account sospeso')->view('email.user_suspended', ['user' => $this->user]);
    }
}
