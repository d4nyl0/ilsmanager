<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $casts = [
        'answer' => 'array',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
