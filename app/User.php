<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

use Log;
use Mail;

use App\Config;

class User extends Authenticatable
{
    use Notifiable;

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function configs()
    {
        return $this->morphMany('App\Config', 'subject');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function fees()
    {
        return $this->hasMany('App\Fee');
    }

    public function refunds()
    {
        return $this->hasMany('App\Refund');
    }

    public function getPrintableNameAttribute()
    {
        return sprintf('%s %s', $this->surname, $this->name);
    }

    public function getFullAddressAttribute()
    {
        return sprintf("%s\n%s (%s)", $this->address_street, $this->address_place, $this->address_prov);
    }

    public function getCustomEmailAttribute()
    {
        if (Config::getConfig('custom_email_aliases') == '1') {
            return sprintf('%s@%s', $this->username, Config::getConfig('custom_email_domain'));
        }
        else {
            return null;
        }
    }

    public function getHumanTypeAttribute()
    {
        $types = self::types();

        foreach($types as $t) {
            if ($t->identifier == $this->type)
                return $t->label;
        }

        Log::error('Tipo di utente non riconosciuto: ' . $this->type);
        return '';
    }

    public function getHumanStatusAttribute()
    {
        $statuses = self::statuses();

        foreach($statuses as $s) {
            if ($s->identifier == $this->status)
                return $s->label;
        }

        Log::error('Stato utente non riconosciuto: ' . $this->status);
        return '';
    }

    public function feeAmount()
    {
        static $value = 0;

        if ($value == 0)
            $value = Config::getConfig($this->type . '_annual_fee');

        return $value;
    }

    public function firstMissingFeeYear()
    {
        $this_year = date('Y');

        $last_fee = $this->fees()->orderBy('year', 'desc')->first();
        if ($last_fee) {
            /*
                Qui c'è un errore deliberato, dovrebbe essere
                if ($last_fee->year < $this_year)
                Viene temporaneamente mantenuto così fino al rinnovo dello
                Statuto, sarà da correggere nel prossimo futuro
            */
            if ($last_fee < $this_year)
                return $this_year;
            else
                return $last_fee->year + 1;
        }
        else {
            /*
                I nuovi iscritti (dunque: senza nessuna quota precedente) che si
                iscrivono da ottobre hanno la quota valida per l'anno successivo
            */
            $this_month = date('m');
            if ($this_month < 10) {
                return $this_year;
            }
            else {
                return $this_year + 1;
            }
        }
    }

    public function regularFee()
    {
        return ($this->fees()->where('year', '>=', date('Y'))->first() != null);
    }

    public function hasRole($roles)
    {
        if (is_array($roles) == false) {
            $roles = [$roles];
        }

        foreach($this->roles as $user_role) {
            foreach($roles as $search_role) {
                if ($user_role->name == $search_role) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function types()
    {
        return [
            (object) [
                'identifier' => 'regular',
                'label' => 'Ordinario'
            ],
            (object) [
                'identifier' => 'association',
                'label' => 'Associazione'
            ],
        ];
    }

    public static function statuses()
    {
        return [
            (object) [
                'identifier' => 'pending',
                'label' => 'In Attesa'
            ],
            (object) [
                'identifier' => 'active',
                'label' => 'Attivo'
            ],
            (object) [
                'identifier' => 'suspended',
                'label' => 'Sospeso'
            ],
            (object) [
                'identifier' => 'expelled',
                'label' => 'Espulso'
            ],
            (object) [
                'identifier' => 'dropped',
                'label' => 'Scaduto'
            ],
        ];
    }

    public function sendMail($class)
    {
        $user = $this;

        try {
            $custom = $user->custom_email;
            if (!empty($custom)) {
                Mail::to($user->email)->cc($custom)->send(new $class($user));
            }
            else {
                Mail::to($user->email)->send(new $class($user));
            }
        }
        catch(\Exception $e) {
            Log::error('Impossibile inoltrare mail di notifica a utente ' . $user->id . ': ' . $e->getMessage());
        }
    }

    public function getConfig($name)
    {
        $c = $this->configs()->where('name', $name)->first();
        if (is_null($c))
            return null;
        else
            return $c->value;
    }

    public function setConfig($name, $value)
    {
        $c = $this->configs()->where('name', $name)->first();
        if (is_null($c)) {
            $c = new Config();
            $c->name = $name;
            $c->subject_id = $this->id;
            $c->subject_type = 'App\User';
        }

        $c->value = $value;
        $c->save();
    }

    /******************************************************* CanResetPassword */

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));

        $custom = $this->custom_email;
        if (!empty($custom)) {
            try {
                $real_email = $this->email;
                $this->email = $custom;
                $this->notify(new ResetPasswordNotification($token));
                $this->email = $real_email;
            }
            catch(\Exception $e) {
                Log::debug('Fallito invio recupero password a indirizzo email personalizzato: ' . $custom);
            }
        }
    }
}
