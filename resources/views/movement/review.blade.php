@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <p>
                I movimenti marcati come "Quote Associative" sono automaticamente elaborati per estrapolare più quote pagate insieme per l'utente selezionato. L'eventuale disavanzo viene contato come "Donazione".
            </p>
            <p>
                Correggere manualmente i movimenti legati a fatture dei fornitori, nella causale specifica il numero di fattura e magari la motivazione.
            </p>
        </div>
    </div>

    @if($pendings->isEmpty() == false)
        <div class="row">
            <div class="col-md-12">
                <form method="POST" action="{{ route('movement.update', 0) }}">
                    @method('PUT')
                    @csrf

                    @foreach($pendings as $movement)
                        @include('movement.editblock', ['movement' => $movement])
                        <br>
                    @endforeach

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-primary">Salva</button>
                        </div>
                    </div>
                </form>

                {!! $pendings->links() !!}
            </div>
        </div>
    @endif
</div>
@endsection
