<li class="list-group-item {{ $account && $account->archived ? 'disabled' : '' }}">
    <div class="row">
        <input type="hidden" name="account_id[]" value="{{ $account ? $account->id : 0 }}">

        <div class="col-md-{{ 7 - $deep }} offset-md-{{ $deep }}">
            <input type="text" class="form-control" name="account_name[]" placeholder="{{ $account ? $account->name : 'Nuovo Account' }}" value="{{ $account ? $account->name : '' }}">
        </div>
        <div class="col-md-1">
            <input type="checkbox" name="account_archived[]" {{ $account && $account->archived ? 'checked' : '' }} value="{{ $account ? $account->id : 0 }}">
        </div>
        <div class="col-md-3">
            @include('account.select', [
                'name' => 'account_parent[]',
                'select' => $account ? $account->parent_id : 0,
                'hide_archived' => false
            ])
        </div>
        <div class="col-md-1">
            <input type="checkbox" name="account_removed[]" value="{{ $account ? $account->id : 0 }}">
        </div>
    </div>
</li>

@if($account)
    @foreach($account->children as $child)
        @include('account.edit', [
            'account' => $child,
            'deep' => $deep + 1
        ])
    @endforeach
@endif
