<div class="form-group row">
    <label for="city" class="col-sm-4 col-form-label">Città</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="city" value="{{ $object ? $object->city : '' }}">
    </div>
</div>
<div class="form-group row">
    <label for="prov" class="col-sm-4 col-form-label">Provincia</label>
    <div class="col-sm-8">
        @include('commons.provinces', ['name' => 'prov', 'value' => $object ? $object->prov : null])
    </div>
</div>
