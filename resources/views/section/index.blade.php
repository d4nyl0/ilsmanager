@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\Section::class)
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createSection">Registra Nuova</button>
                <div class="modal fade" id="createSection" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Sezione</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('section.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('section.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endcan

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Città</th>
                        <th>Provincia</th>
                        <th>Soci</th>
                        <th>Volontari</th>
                        <th>Disponibilità</th>

                        @if($currentuser->hasRole(['admin', 'referent']))
                            <th>Modifica</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $section)
                        <tr>
                            <td>{{ $section->city }}</td>
                            <td>{{ $section->prov }}</td>
                            <td>{{ $section->users()->count() }}</td>
                            <td>{{ $section->volunteers()->count() }}</td>
                            <td>{{ $section->balance }} €</td>

                            @if($currentuser->hasRole(['admin', 'referent']))
                                <td>
                                    @can('update', $section)
                                        <a href="{{ route('section.edit', $section->id) }}"><span class="oi oi-pencil"></span></a>
                                    @endcan
                                </td>
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
