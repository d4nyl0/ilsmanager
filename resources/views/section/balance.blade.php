@extends('layouts.app')

@section('content')
<div class="container">
    <nav class="nav">
        @foreach(App\EconomicLog::selectRaw('YEAR(created_at) as year')->where('section_id', $section->id)->distinct()->orderBy('year', 'asc')->pluck('year') as $nav_year)
            <a class="nav-link {{ $year == $nav_year ? 'active' : '' }}" href="{{ route('section.balance', ['id' => $section->id, 'year' => $nav_year]) }}">{{ $nav_year }}</a>
        @endforeach
    </nav>

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Valore</th>
                        <th>Causale</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                        <tr>
                            <td>{{ $row->created_at }}</td>
                            <td>{{ $row->amount }}</td>
                            <td>{{ $row->reason }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
