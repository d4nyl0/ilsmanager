@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser">Nuovo Rimborso Spese</button>
            <div class="modal fade" id="createUser" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Nuovo Rimborso Spese</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('refund.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                @include('refund.form', ['object' => null])
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-primary">Salva</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($currentuser->hasRole('admin'))
        <?php
        $users = App\User::whereIn('status', ['active'])->has('refunds')->orderBy('username', 'asc')->get();
        ?>
    @endif

    @if($currentuser->hasRole('admin') && app('request')->input('user_id'))
        <?php
        $objects = $objects->where('user_id', app('request')->input('user_id'));
        ?>
    @endif

    @if($currentuser->hasRole('admin'))
        <form method="GET" action="" class="auto-filter-form">
            <div class="row mt-3">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="user_id">Socio:</label>
                        <select id="user_id" name="user_id" class="form-control">
                            <option value="">Tutti</option>

                            @foreach($users as $user)
                                <option value="{{ $user->id }}" @selected(app('request')->input('user_id') == $user->id)>{{ $user->printable_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </form>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Data</th>
                        <th>Ammontare</th>
                        <th>Socio</th>
                        <th>Sezione</th>
                        <th>Dettagli</th>
                    </tr>
                </thead>
                <tbody>
                    @php $total = 0; @endphp
                    @foreach($objects as $refund)
                        @php $total += $refund->amount; @endphp
                        <tr class="{{ $refund->refunded ? 'table-success' : 'table-danger' }}">
                            <td>{{ $refund->id }}</td>
                            <td>{{ $refund->date }}</td>
                            <td class="text-right">{{ $refund->amount }}</td>
                            <td>{{ $refund->user ? $refund->user->printable_name : '' }}</td>
                            <td>{{ $refund->section ? $refund->section->city : '' }}</td>
                            <td>
                                <a href="{{ route('refund.edit', $refund->id) }}"><span class="oi oi-pencil"></span></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="2" class="text-right">Totale</td>
                        <td class="text-right">{{ number_format($total, 2) }}</td>
                        <td colspan="3"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
