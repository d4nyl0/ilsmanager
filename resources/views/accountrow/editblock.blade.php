<?php

$item_class = '';

// check if a related AccountRow was created for this Movement
if ($ar) {

    if ($ar->account_id == 0)
        $item_class = 'list-group-item-danger';
    else if (!$ar->exists)
        $item_class = 'list-group-item-warning';
}
else {
    $item_class = 'list-group-item-danger';
}

?>

<li class="list-group-item {{ $item_class }}">
    <input type="hidden" name="movement[]" value="{{ $movement->id }}">
    <input type="hidden" name="account_row[]" value="{{ $ar && $ar->exists ? $ar->id : 'new' }}">

    <div class="row">
        <div class="col-md-3">
            <input class="form-control" type="number" name="amount[]" value="{{ $ar ? $ar->amount : 0 }}" step="0.01">
        </div>
        <div class="col-md-3">
            @include('account.select', ['select' => $ar ? $ar->account_id : 0])
        </div>
        <div class="col-md-3 ils-container-user-select">
            <div>
                @include('user.select', ['select' => $ar ? $ar->user_id : 0, 'status' => ['pending', 'active', 'suspended'], 'extra_classes' => 'ils-user-select'])
            </div>
            <div class="d-none text-right ils-container-user-info" data-userbaseurl="{{ route('user.edit', 666) }}"><!-- please keep 666 -->
		<p><a class="ils-user-link" href="#" target="_blank">
                   <span class="oi oi-person"></span>
                   <span class="ils-user-displayname"></span>
                </a></p>
            </div>
        </div>
        <div class="col-md-3">
            @include('section.select', ['select' => $ar ? $ar->section_id : 0])
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-md-10">
            <input class="form-control" type="text" name="notes[]" value="{{ $ar ? $ar->notes : '' }}" placeholder="Causale">
        </div>
        <div class="col-md-2 text-right">
            <button class="btn btn-danger delete-row">Elimina</button>
        </div>
    </div>
</li>
