@extends('layouts.app')

@section('content')
<div class="container">
    @can('create', App\User::class)
        <div class="row">
            <div class="col-md-12">
                <h6>I soci vengono approvati una volta l'anno durante l'assemblea per via dello statuto, l'accettazione delle quote é manuale e avviene periodicamente verificando i versamenti sul conto bancario o PayPal.</h6>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser">Registra Nuovo</button>
                <div class="modal fade" id="createUser" tabindex="-1" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Registra Socio</h5>
                                <button type="button" class="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <form method="POST" action="{{ route('user.store') }}">
                                @csrf
                                <div class="modal-body">
                                    @include('user.form', ['object' => null])
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary">Salva</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif


    @if($currentuser->hasRole('admin'))
            <div class="row mt-3">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            <ul class="nav nav-tabs card-header-tabs" id="user-charts" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="#age-users" role="tab" aria-controls="age-users" aria-selected="true">Età soci</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link"  href="#prov" role="tab" aria-controls="prov" aria-selected="false">Province dei soci</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#sections" role="tab" aria-controls="sections" aria-selected="false">Sezioni locali</a>
                                </li>
                            </ul>
                        </div>

                        @php
                        $year = array();
                        $by_year = array();
                        $prov = array();
                        $by_prov = array();
                        $section_by = array();
                        $section = array();
                        $section_by = array();
                        foreach(App\User::where('status', 'active')->with('section')->get() as $user) {
                            $_year = date('Y', strtotime($user->birth_date));
                            $year[$_year] = $_year;
                            if (!isset($by_year[$_year])) {
                                $by_year[$_year] = 1;
                            } else {
                                $by_year[$_year]++;
                            }

                            $_prov = $user->birth_prov;
                            if ($user->address_prov) {
                                $_prov = $user->address_prov;
                            }
                            $prov[$_prov] = $_prov;

                            if (!isset($by_prov[$_prov])) {
                                $by_prov[$_prov] = 1;
                            } else {
                                $by_prov[$_prov]++;
                            }

                            $_section = $user->section->city ?? null;
                            if ($_section) {
                                $section[$_section] = $_section;

                                if (!isset($section_by[$_section])) {
                                    $section_by[$_section] = 1;
                                } else {
                                    $section_by[$_section]++;
                                }
                            }
                        }

                        @endphp

                        <div class="card-body">
                            <div class="tab-content mt-3">
                                <div class="tab-pane active" id="age-users" role="tabpanel">
                                    <div>
                                        <canvas id="age-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('age-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $year ) ),
                                                datasets: [{
                                                    label: '# per anno di nascita',
                                                    data: @json( array_values( $by_year ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                                <div class="tab-pane" id="prov" role="tabpanel">
                                    <div>
                                        <canvas id="prov-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('prov-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $prov ) ),
                                                datasets: [{
                                                    label: '# per provincia',
                                                    data: @json( array_values( $by_prov ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                                <div class="tab-pane" id="sections" role="tabpanel">
                                    <div>
                                        <canvas id="sections-chart"></canvas>
                                    </div>
                                    <script>
                                    addEventListener("DOMContentLoaded", (event) => {
                                        const ctx = document.getElementById('sections-chart');

                                        new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: @json( array_values( $section ) ),
                                                datasets: [{
                                                    label: '# per sezione',
                                                    data: @json( array_values( $section_by ) ),
                                                    borderWidth: 1
                                                }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                },
                                                ticks: {
                                                    precision:0
                                                }
                                            }
                                        });
                                    });
                                    </script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    @endif

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="alert alert-info">
                Ci sono attualmente {{ App\User::where('status', 'active')->where('type', 'regular')->count() }} persone e {{ App\User::where('status', 'active')->where('type', 'association')->count() }} associazioni iscritte.
            </div>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <ul class="nav nav-tabs card-header-tabs" id="user-list" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#all" role="tab" aria-controls="all" aria-selected="true">Tutti i soci attivi ({{ App\User::where('status', 'active')->count() }})</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"  href="#to-approve" role="tab" aria-controls="to-approve" aria-selected="false">Soci in Attesa di Approvazione ({{ App\User::where('status', 'pending')->count() }})</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#others" role="tab" aria-controls="others" aria-selected="false">Tutti gli altri  ({{ App\User::where('status', '!=', 'active')->where('status', '!=', 'pending')->count() }})</a>
                        </li>
                    </ul>
                </div>

                <div class="card-body">
                    <div class="tab-content mt-3">
                        <div class="tab-pane active" id="all" role="tabpanel">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Cognome</th>
                                        <th>Nickname</th>
                                        <th>Sezione</th>

                                        @if($currentuser->hasRole('admin'))
                                            <th>Quota</th>
                                            <th>Stato</th>
                                            <th>Modifica</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(App\User::where('status', 'active')->orderBy('surname')->get() as $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->surname }}</td>
                                            <td>{{ $user->expelled_at ? 'ESPULSO' : $user->username }}</td>
                                            <td>{{ $user->section ? $user->section->city : '' }}</td>

                                            @if($currentuser->hasRole('admin'))
                                                <td>{{ $user->fees()->max('year') }}</td>
                                                <td>{{ $user->human_status }}</td>
                                                <td><a href="{{ route('user.edit', $user->id) }}"><span class="oi oi-pencil"></span></a></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane" id="to-approve" role="tabpanel">
                            @if(App\User::where('status', 'pending')->orderBy('surname')->count() > 0)
                            <form method="POST" action="{{ route('user.approve') }}">
                                @csrf

                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Nome</th>
                                            <th>Cognome</th>
                                            <th>Nickname</th>
                                            <th>Sezione</th>
                                            <th>Domanda</th>
                                            @if($currentuser->hasRole('admin'))
                                            <th>Quota Pagata</th>
                                            <th>Modifica</th>
                                            <th>Approva</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach(App\User::where('status', 'pending')->orderBy('surname')->get() as $user)
                                            <tr>
                                                <td>{{ $user->name }}</td>
                                                <td>{{ $user->surname }}</td>
                                                <td>{{ $user->username }}</td>
                                                <td>{{ $user->section ? $user->section->city : '' }}</td>
                                                <td>{{ $user->request_at }}</td>
                                                @if($currentuser->hasRole('admin'))
                                                <td>{{ $user->fees()->count() > 0 ? 'SI' : 'NO' }}</td>
                                                <td><a href="{{ route('user.edit', $user->id) }}"><span class="oi oi-pencil"></span></a></td>
                                                <td><input type="checkbox" name="approved[]" value="{{ $user->id }}"></td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                @if($currentuser->hasRole('admin'))
                                <div class="form-group row">
                                    <div class="col-sm-10">
                                        <button type="submit" class="btn btn-primary">Salva</button>
                                    </div>
                                </div>
                                @endif
                            </form>
                            @endif
                        </div>
                        <div class="tab-pane" id="others" role="tabpanel">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>Cognome</th>
                                        <th>Nickname</th>
                                        <th>Sezione</th>

                                        @if($currentuser->hasRole('admin'))
                                            <th>Quota</th>
                                            <th>Stato</th>
                                            <th>Modifica</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach(App\User::where('status', '!=', 'active')->where('status', '!=', 'pending')->orderBy('surname')->get() as $user)
                                        <tr>
                                            <td>{{ $user->name }}</td>
                                            <td>{{ $user->surname }}</td>
                                            <td>{{ $user->expelled_at ? 'ESPULSO' : $user->username }}</td>
                                            <td>{{ $user->section ? $user->section->city : '' }}</td>

                                            @if($currentuser->hasRole('admin'))
                                                <td>{{ $user->fees()->max('year') }}</td>
                                                <td>{{ $user->human_status }}</td>
                                                <td><a href="{{ route('user.edit', $user->id) }}"><span class="oi oi-pencil"></span></a></td>
                                            @endif
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
