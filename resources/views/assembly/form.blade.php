<div class="form-group row">
    <label for="date" class="col-sm-4 col-form-label">Data</label>
    <div class="col-sm-8">
        <input type="date" class="form-control" name="date" value="{{ $object ? $object->date : '' }}">
    </div>
</div>

<div class="form-group row">
    <label for="status" class="col-sm-4 col-form-label">Stato</label>
    <div class="col-sm-8">
        <select class="form-control" name="status" autocomplete="off">
            <option value="pending" {{ $object && $object->status == 'pending' ? 'selected' : '' }}>In Attesa</option>
            <option value="open" {{ $object && $object->status == 'open' ? 'selected' : '' }}>Aperta</option>
            <option value="closed" {{ $object && $object->status == 'closed' ? 'selected' : '' }}>Chiusa</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="announce" class="col-sm-4 col-form-label">Convocazione</label>
    <div class="col-sm-8">
        <textarea class="form-control" name="announce" rows="10">{{ $object ? $object->announce : '' }}</textarea>
    </div>
</div>
