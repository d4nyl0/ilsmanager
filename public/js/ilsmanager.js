$(function() {
    $('.show-all').click(function() {
        var target = $(this).attr('data-target');
        $(target).find('.hidden').toggle();
    });

    $('.add-row').click(function(e) {
        e.preventDefault();
        var row = $(this).siblings('.hidden').find('li').clone();
        $(this).closest('.list-group-item').before(row);
    });

    $('.delete-row').click(function(e) {
        e.preventDefault();
        $(this).closest('.list-group-item').remove();
    });

    $('.multiple-files').on('change', 'input:file', function() {
        $(this).after('<input type="file" name="file[]" autocomplete="off">');
    });

    $("input.date").inputmask({
        'mask': '9999-99-99',
        'placeholder': 'yyyy-mm-dd',
        'clearIncomplete': true,
    });

    /**
     * Allow to refresh User info when one is selected
     *
     * This just removes the 'd-none' class to hide/show it using jQuery.
     *
     * https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/18
     */
    $('.ils-container-user-select .ils-container-user-info')
        .hide()
        .removeClass('d-none');

    /**
     * Allow to refresh User info when one is selected
     *
     * When an User is selected, refresh the info box below.
     * At the moment probably this is just used by this file:
     * resources/views/accountrow/editblock.blade.php
     *
     * https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/18
     */
    $('.ils-container-user-select .ils-user-select')
      .change(function() {
        var $container = $(this).closest('.ils-container-user-select');
        var $selected  = $(this).find(':selected');
        var $userInfo  = $container.find('.ils-container-user-info');
        var userId     = $selected.val();
        var userName   = $selected.text();
        var userBase   = $userInfo.data('userbaseurl');
        if( userId && userId != 0 ) {

            // in the template there is a fake Id just to be used as template
            var userURL = userBase.replace(666, userId);

            $userInfo.find('.ils-user-link')
                     .prop('href', userURL);

            $userInfo.find('.ils-user-displayname')
                     .text(userName);

            $userInfo.show('fast');
        } else {
            $userInfo.hide('fast');
        }
      })
      .change();

    $('body').on('change', '.max-selections input:checkbox', function(e) {
        e.preventDefault();
        var container = $(this).closest('.max-selections');
        var max = parseInt(container.attr('data-max-selections'));
        var choosen = container.find('input:checkbox').filter(':checked').length;

        if (choosen >= max) {
            container.find('input:checkbox').filter(':not(:checked)').prop('disabled', true);
        }
        else {
            container.find('input:checkbox').prop('disabled', false);
        }
    });

    $('.auto-filter-form select').change(function() {
        $(this).closest('form').submit();
    });

    if ($.fn.DataTable) {
        jQuery('.table').DataTable({
            paging: false,
            language: {
                url: '/js/datatables-it-IT.json',
            }
        });
    }

    $(document).keypress(function(e) {
        if ($(".modal").hasClass('show') && (e.keycode == 13 || e.which == 13)) {
            if (!$(".modal textarea").is(":focus")) {
                $(".modal .btn-primary").trigger('click');
            }
        }
    });

    $('#user-charts a, #user-list a').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show');
    })
});
