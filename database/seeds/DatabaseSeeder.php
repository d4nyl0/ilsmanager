<?php

use App\Assembly;
use App\Votation;
use App\Vote;
use Illuminate\Database\Seeder;


use App\Movement;
use App\Role;
use App\User;
use App\Account;
use App\AccountRule;
use App\AccountRow;
use App\Bank;
use App\Section;
use App\Fee;
use App\Refund;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::all()->isEmpty()) {
            $admin_role = new Role();
            $admin_role->name = 'admin';
            $admin_role->save();

            $member_role = new Role();
            $member_role->name = 'member';
            $member_role->save();

            $referent_role = new Role();
            $referent_role->name = 'referent';
            $referent_role->save();
        }

        if (Section::all()->isEmpty()) {
            $section_roma = new Section();
            $section_roma->city = 'Roma';
            $section_roma->prov = 'RM';
            $section_roma->save();

            $section_milano = new Section();
            $section_milano->city = 'Milano';
            $section_milano->prov = 'MI';
            $section_milano->save();
        }

        if (Account::all()->isEmpty()) {
            $accounts = [
                'Soci' => [
                    'Quote Associative'
                ],
                'Attività Istituzionale' => [
                    'Sponsorizzazioni',
                    'Donazioni'
                ],
                'Beni e Servizi' => [
                    'Stampa',
                    'Materiale Promozionale',
                    'Posta e Spedizioni',
                    'Banche',
                    'Commercialista',
                    'Consulenze',
                    'Servizi Web',
                    'Hardware',
                ],
                'Imposte e Tasse' => []
            ];

            foreach ($accounts as $parent => $sub_accounts) {
                $p = new Account();
                $p->name = $parent;
                $p->parent_id = null;
                $p->save();

                foreach ($sub_accounts as $sa) {
                    $s = new Account();
                    $s->name = $sa;
                    $s->parent_id = $p->id;
                    $s->save();
                }
            }

            Account::where('name', 'Quote Associative')->update(['fees' => true]);
            Account::where('name', 'Donazioni')->update(['donations' => true]);
            Account::where('name', 'Banche')->update(['bank_costs' => true]);
        }

        $time = strtotime("-1 year", time());
        $previous_year = date("Y", $time);

        if (User::all()->isEmpty()) {
            $user = new User();
            $user->username = 'admin';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Amministratore';
            $user->surname = 'ILS';
            $user->email = 'admin@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->section_id = $section_roma->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "1970-01-01";
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (1,1)");

            $fee = new Fee();
            $fee->account_row_id = Account::where('name', 'Quote Associative')->first()->id;
            $fee->user_id = $user->id;
            $fee->year = $previous_year;
            $fee->save();

            $fee = new Fee();
            $fee->account_row_id = Account::where('name', 'Quote Associative')->first()->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();

            $user = new User();
            $user->username = 'member';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Membro';
            $user->surname = 'ILS';
            $user->email = 'member@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->address_prov = 'RM';
            $user->section_id = $section_roma->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "1990-01-01";
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,2)");

            $fee = new Fee();
            $fee->account_row_id = Account::where('name', 'Quote Associative')->first()->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();

            $user = new User();
            $user->username = 'referent';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Referente';
            $user->surname = 'ILS';
            $user->email = 'referent@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->address_prov = 'MI';
            $user->section_id = $section_milano->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "1980-01-01";
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (3,3)");

            $fee = new Fee();
            $fee->account_row_id = Account::where('name', 'Quote Associative')->first()->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();

            $user = new User();
            $user->username = 'pending';
            $user->password = Hash::make($user->username);
            $user->status = 'pending';
            $user->type = 'regular';
            $user->name = 'In attesa';
            $user->surname = 'ILS';
            $user->email = 'pending@example.com';
            $user->notes = '';
            $user->birth_prov = 'MI';
            $user->address_prov = 'MI';
            $user->section_id = $section_milano->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "1995-01-01";
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,4)");

            $fee = new Fee();
            $fee->account_row_id = Account::where('name', 'Quote Associative')->first()->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();

            $user = new User();
            $user->username = 'suspended';
            $user->password = Hash::make($user->username);
            $user->status = 'suspended';
            $user->type = 'regular';
            $user->name = 'Sospeso';
            $user->surname = 'ILS';
            $user->email = 'suspended@example.com';
            $user->notes = '';
            $user->birth_prov = 'MI';
            $user->address_prov = 'RM';
            $user->section_id = $section_roma->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,5)");

            $fee = new Fee();
            $fee->account_row_id = Account::where('name', 'Quote Associative')->first()->id;
            $fee->user_id = $user->id;
            $fee->year = 2020;
            $fee->save();

            $user = new User();
            $user->username = 'expelled';
            $user->password = Hash::make($user->username);
            $user->status = 'expelled';
            $user->type = 'regular';
            $user->name = 'Espulso';
            $user->surname = 'ILS';
            $user->email = 'expelled@example.com';
            $user->notes = '';
            $user->birth_prov = 'FI';
            $user->address_prov = 'RM';
            $user->section_id = $section_roma->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,6)");

            $user = new User();
            $user->username = 'dropped';
            $user->password = Hash::make($user->username);
            $user->status = 'dropped';
            $user->type = 'regular';
            $user->name = 'Eliminato';
            $user->surname = 'ILS';
            $user->email = 'dropped@example.com';
            $user->notes = '';
            $user->section_id = $section_milano->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,7)");

            $user = new User();
            $user->username = 'association';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Associazione';
            $user->surname = 'ILS';
            $user->email = 'association@example.com';
            $user->notes = '';
            $user->birth_prov = 'FI';
            $user->address_prov = 'MI';
            $user->section_id = $section_milano->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "2000-01-01";
            $user->save();
            DB::insert("INSERT INTO role_user (role_id, user_id) VALUES (2,8)");

            $fee = new Fee();
            $fee->account_row_id = Account::where('name', 'Quote Associative')->first()->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();
        }

        if (Bank::all()->isEmpty()) {
            $bank = new Bank();
            $bank->name = 'Unicredit';
            $bank->type = 'unicredit';
            $bank->identifier = 'IT74G0200812609000100129899';
            $bank->save();

            $r = new AccountRule();
            $r->bank_id = $bank->id;
            $r->rule = 'IMPOSTA BOLLO CONTO CORRENTE';
            $r->account_id = 10;
            $r->notes = 'Imposta bollo';
            $r->save();

            $movement = new Movement();
            $movement->amount = 100;
            $movement->bank_id = $bank->id;
            $movement->date = date('Y-m-d');
            $movement->identifier = '';
            $movement->notes = 'IMPOSTA BOLLO CONTO CORRENTE';
            $movement->save();

            $ar = new AccountRow();
            $ar->movement_id = $movement->id;
            $ar->account_id = Account::where('bank_costs', true)->first()->id;
            $ar->notes = 'IMPOSTA BOLLO CONTO CORRENTE';
            $ar->amount_in = 100;
            $ar->save();

            $bank = new Bank();
            $bank->name = 'PayPal';
            $bank->type = 'paypal';
            $bank->identifier = 'direttore@linux.it';
            $bank->save();

            $r = new AccountRule();
            $r->bank_id = $bank->id;
            $r->rule = '^Hetzner Online';
            $r->account_id = 13;
            $r->notes = 'VPS';
            $r->save();

            $movement = new Movement();
            $movement->amount = 200;
            $movement->bank_id = $bank->id;
            $movement->date = date('Y-m-d');
            $movement->identifier = '';
            $movement->notes = 'Hetzner Online Fattura';
            $movement->save();

            $movement = new Movement();
            $movement->amount = 10;
            $movement->bank_id = $bank->id;
            $movement->date = date('Y-m-d');
            $movement->identifier = '';
            $movement->notes = 'Rimborso Milano';
            $movement->save();

            $ar = new AccountRow();
            $ar->movement_id = $movement->id;
            $ar->user_id = 3; // Utente referente
            $ar->account_id = Account::where('name', 'Sponsorizzazioni')->first()->id;
            $ar->notes = 'Rimborso sede Milano';
            $ar->amount_in = 10;
            $ar->section_id = $section_milano->id;
            $ar->save();

            $refund = new Refund();
            $refund->amount = 20;
            $refund->user_id = 3; // Utente referente
            $refund->section_id = $section_milano->id;
            $refund->date = date('Y-m-d');
            $refund->refunded = 1;
            $refund->notes = 'Rimborso fatto Milano';
            $refund->save();

            $movement = new Movement();
            $movement->amount = 20;
            $movement->bank_id = $bank->id;
            $movement->date = date('Y-m-d');
            $movement->identifier = '';
            $movement->notes = 'Spesa Roma';
            $movement->save();

            $ar = new AccountRow();
            $ar->movement_id = $movement->id;
            $ar->user_id = 2; // Utente regolare
            $ar->account_id = Account::where('name', 'Sponsorizzazioni')->first()->id;
            $ar->notes = 'Spesa sede Roma';
            $ar->amount_out = 20;
            $ar->section_id = $section_roma->id;
            $ar->save();
        }

        if (Assembly::all()->isEmpty()) {
            $assembly = new Assembly();
            $assembly->date = '2099-12-31';
            $assembly->status = 'pending';
            $assembly->announce = 'Assemblea futura';
            $assembly->save();

            $assembly = new Assembly();
            $date = new DateTime(now());
            $interval = new DateInterval('P1M');
            $date->add($interval);
            $assembly->date = $date->format('Y-m-d');
            $assembly->status = 'open';
            $assembly->announce = 'Assemblea attiva';
            $assembly->save();

            $assembly = new Assembly();
            $assembly->date = "0001-01-01";
            $assembly->status = 'closed';
            $assembly->announce = 'Assemblea chiusa';
            $assembly->save();
        }

        if (Votation::all()->isEmpty()) {
            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione in attesa';
            $votation->status = 'pending';
            $votation->options = array_filter(array("a","b","c"));
            $votation->save();

            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione in corso';
            $votation->status = 'running';
            $votation->options = array_filter(array("d","e","f"));
            $votation->save();

            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione chiusa';
            $votation->status = 'closed';
            $votation->options = array_filter(array("g","h","i"));
            $votation->save();
        }

        if (Vote::all()->isEmpty()) {
            $vote = new Vote();
            $vote->votation_id = 3;
            $vote->user_id = 1;
            $vote->answer = array_filter(array("g"));
            $vote->save();

            $vote = new Vote();
            $vote->votation_id = 3;
            $vote->user_id = 2;
            $vote->answer = array_filter(array("h"));
            $vote->save();

            $vote = new Vote();
            $vote->votation_id = 3;
            $vote->user_id = 3;
            $vote->answer = array_filter(array("i"));
            $vote->save();

            $vote = new Vote();
            $vote->votation_id = 3;
            $vote->user_id = 4;
            $vote->answer = array_filter(array("g"));
            $vote->save();
        }
    }
}
